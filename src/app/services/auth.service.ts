import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import {Storage} from '@ionic/storage';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public token: string;
  guichetier=false;
  admin;
  user={
    username:'',
    roles:[],
    accessDenied:false
  }
  jwtHelper = new JwtHelperService;
  public authenticate: boolean;
  
  constructor(private httpClient: HttpClient,private storage: Storage) { }
  login(username:string,password:string){//ne pas factoriser
    const data={
      username: username,
      password: password
    };
    return new Promise(
      (resolve, reject)=>{
      this.httpClient
        .post<any>('http://localhost:8000/api/logincheck',data)
        .subscribe(
          (rep)=>{
            this.token = rep.token
            this.storage.set('token', this.token);
            const tokenDeco=this.jwtHelper.decodeToken(this.token);
            this.user.username=tokenDeco.username;
            this.user.roles=tokenDeco.roles;
            this.Poste();
            this.authenticate = true;
            resolve();
          },
          (error)=>{
            this.authenticate = false;
            reject(error);
          }
        );
      })
  }
  public loadToken() {
    this.storage.get('token').then(token=>{
      this.token = token
      if (this.token) {
        this.authenticate = true;
      } else {
        this.authenticate = false;
      }
      this.storage.get('user').then(user=>this.user=user);
      return this.authenticate;
    });
  }
  logout() {
    this.authenticate=false;
    this.storage.remove("token");
    this.storage.remove("user");
  }
  Poste(){
    if(this.user.roles[0].search('ROLE_SUPERADMIN')>=0 || this.user.roles[0].search('ROLE_ANNONCEUR')>=0 || this.user.roles[0].search('ROLE_CANDIDAT')>=0 ){
      this.user.accessDenied=true;
      this.logout();
    }
}
}